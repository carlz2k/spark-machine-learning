name := "spark-machine-learning"

version := "1.0"

val sparkVersion = "1.6.2"
val scalaVer = "2.11.8"

scalaVersion := scalaVer



libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % sparkVersion,
  "org.apache.spark" %% "spark-sql" % sparkVersion,
  "org.apache.spark" %% "spark-mllib" % sparkVersion,
  "org.twitter4j" % "twitter4j-core" % "4.0.4",
  "com.databricks" % "spark-csv_2.11" % "1.4.0",
  "org.scalatest" % "scalatest_2.11" % "3.0.0-M16-SNAP6" % "test",
  "junit" % "junit" % "4.12" % "test"
)

