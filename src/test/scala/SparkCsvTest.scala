import java.io.File

import com.google.common.base.Strings
import com.typesafe.config.ConfigFactory
import org.apache.commons.io.FileUtils
import org.apache.spark.mllib.classification.LogisticRegressionWithLBFGS
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.sql.{Row, SQLContext}
import org.apache.spark.sql.types.{StringType, StructField, StructType}
import org.apache.spark.{SparkConf, SparkContext}

object SparkCsvTest extends App {
  def stringToDouble(x: String): Double = {
    if (x != "" && x != null) {
      x.toDouble
    } else {
      0.0
    }
  }

  val LABELS = "Issue Type,Key,Summary,Assignee,Reporter,Priority,Status,Resolution,Created,Updated,Due Date,DueTime,Issue Resolution,Original Estimate,Remaining Estimate,? Original Estimate,? Remaining Estimate,Story Points,Velocity %"

  def getIndex(label: String): Int = {
    LABELS.split(",").zipWithIndex.find {
      x => x._1 == label
    }.get._2
  }

  def run(): Unit = {
    val savedLocation = "src\\test\\resources\\new.csv"
    val csvFile = "src\\test\\resources\\jira_issues.csv"

    System.setProperty("hadoop.home.dir", "c:\\hadoop-2.7.2\\")

    val config = ConfigFactory.load()
    val conf = new SparkConf().setAppName(config.getString("spark.master"))
      .setMaster(config.getString("spark.master"))
    val sc = new SparkContext(conf)
    val sqlContext = new SQLContext(sc)

    val headers = LABELS.split(",").map(
      header => new StructField(header, StringType, true)
    )

    val df = sqlContext.read.format("com.databricks.spark.csv").option("header", "false")
      .schema(StructType(headers))
      .load(csvFile)

    val trainingDataTestDataSplit = 0.7

    val splits = df.randomSplit(Array(trainingDataTestDataSplit, 1 - trainingDataTestDataSplit), seed = 11L)
    val trainingData = splits(0)

    val testData = splits(1)

    val model = new LogisticRegressionWithLBFGS().setNumClasses(2).run(trainingData.rdd
      .filter {
        row =>
          !Strings.isNullOrEmpty(row.getString(getIndex("? Original Estimate"))) || !Strings.isNullOrEmpty(row.getString(getIndex("? Remaining Estimate")))
      }.map {
      row => {
        println("line1=" + row)
        val finishedLabel = isFinishedOnTime(row)
        val isLargeProjectParam = isLargeProject(getStoryPoint(row))
        val isLargeEstimateParam = isLargeEsimate(stringToDouble(row.getString(getIndex("? Original Estimate"))))
        println("is finished = " + finishedLabel + " is large project = " + isLargeEstimateParam + " is large est = " + isLargeEstimateParam)
        LabeledPoint(finishedLabel, Vectors.dense(isLargeProjectParam, isLargeEstimateParam))
      }

    })

    // Evaluate model on training examples and compute training error
    val labelAndPreds = testData.rdd
      .filter {
        line =>
          !Strings.isNullOrEmpty(line.getString(getIndex("? Original Estimate"))) || !Strings.isNullOrEmpty(line.getString(getIndex("? Remaining Estimate")))
      }
      .map {
        row =>
          //println("line2=" + line)
          val finishedLabel = isFinishedOnTime(row)
          val isLargeProjectParam = isLargeProject(getStoryPoint(row))
          val isLargeEstimateParam = isLargeEsimate(stringToDouble(row.getString(getIndex("? Original Estimate"))))
          LabeledPoint(finishedLabel, Vectors.dense(isLargeProjectParam, isLargeEstimateParam))
      }
      .map { point =>
        val prediction = model.predict(point.features)
        (point.label, prediction)
      }

    val trainErr = labelAndPreds.filter(r => r._1 != r._2).count.toDouble / testData.count

    println("error = " + trainErr)

    val model2 = new LogisticRegressionWithLBFGS().setNumClasses(2).run(trainingData.rdd
      .filter {
        row =>
          !Strings.isNullOrEmpty(row.getString(getIndex("Assignee"))) && !row.getString(getIndex("Assignee")).equalsIgnoreCase("Unassigned")
      }.map {
      row => {
        println("line1=" + row)
        val finishedLabel = isFinishedOnTime(row)
        LabeledPoint(finishedLabel, Vectors.dense(row.getString(getIndex("Assignee")).hashCode))
      }

    })

    // Evaluate model on training examples and compute training error
    val labelAndPreds2 = testData.rdd
      .filter {
        line =>
          !Strings.isNullOrEmpty(line.getString(getIndex("Assignee"))) && !line.getString(getIndex("Assignee")).equalsIgnoreCase("Unassigned")
      }
      .map {
        row =>
          //println("line2=" + line)
          val finishedLabel = isFinishedOnTime(row)
          LabeledPoint(finishedLabel, Vectors.dense(row.getString(getIndex("Assignee")).hashCode))
      }
      .map { point =>
        val prediction = model2.predict(point.features)
        (point.label, prediction)
      }

    val trainErr2 = labelAndPreds2.filter(r => r._1 != r._2).count.toDouble / testData.count

    println("error = " + trainErr2)
  }

  private def isFinishedOnTime(data: Row): Int = {
    var finishedLabel = 1
    var remainingEstimate = 0
    if (!Strings.isNullOrEmpty(data.getString(getIndex("? Remaining Estimate")))) {
      remainingEstimate = data.getString(getIndex("? Remaining Estimate")).toInt
    }
    var originalEstimate = 0

    if (!Strings.isNullOrEmpty(data.getString(getIndex("? Original Estimate")))) {
      originalEstimate = data.getString(getIndex("? Original Estimate")).toInt
    }

    if (data.getString(getIndex("Status")) != "Done" && remainingEstimate == 0 && originalEstimate != 0) {
      finishedLabel = 0
    }

    finishedLabel
  }

  private def getStoryPoint(data: Row): Int = {
    var storyPoint = 0
    if (!Strings.isNullOrEmpty(data.getString(getIndex("Story Points")))) {
      storyPoint = data.getString(getIndex("Story Points")).toInt
    }
    storyPoint
  }

  private def isLargeProject(storyPoint: Double): Double = {
    if (storyPoint > 5) {
      1.0
    } else {
      0.0
    }
  }

  private def isLargeEsimate(estimate: Double): Double = {
    if (estimate > 160000) {
      1.0
    } else {
      0.0
    }
  }

  run()
}
