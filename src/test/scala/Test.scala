

import scala.collection.mutable
import scala.util.control.Breaks

object Test extends App {

  println(SparkCsvTest.getIndex("Remaining Estimate"))

  val l = List(1, 2, 3, 4, 5)

  def g(v: Int) = {
    val x = List(v -> v * 2)
    x
  }

  val x = l.map(x => g(x))
  println(x)
  val y = l.flatMap(x => g(x))
  println(y)

  //val results = StockScreener.filter(0.1)
  def toInt(s: String): Option[Int] = {
    try {
      Some(Integer.parseInt(s.trim))
    } catch {
      // catch Exception to catch null 's'
      case e: Exception => None
    }
  }

  val s = Seq("1", "2", "foo", "3", "bar")
  println(s.flatMap(toInt))

  def fib2(n: Int): Int = {
    @annotation.tailrec
    def loop(n: Int, prev: Int, cur: Int): Int =
      if (n == 0) prev
      else loop(n - 1, cur, prev + cur)
    loop(n, 0, 1)
  }

  def fib(n: Int): Int = {
    if (n == 0) 0
    else if (n == 1) 1
    else fib(n - 2) + fib(n - 1)
  }

  def isSorted[A](as: Array[A], gt: (A, A) => Boolean): Boolean = {
    def go(n: Int): Boolean = {
      if (n >= as.length - 1) true
      else if (gt(as(n), as(n + 1))) false
      else go(n + 1)
    }
    go(0)
  }

  println(fib2(30))
  println(fib(30))

  //for(result<-results) {
  // println(result.symbol)
  //}
  //Helper.getLastTwoTradingDates
  //getAllDailyQuote
  //val stocks = testStockListDownload()
  //testDailyQuoteDownload(stocks)
  //testHistoricalDataDownload(stocks)

  def findFirstNonRepeatCharacter(s: String): Char = {
    var result = 0.toChar
    val characterCountMap = mutable.HashMap.empty[Char, Integer]
    for (character <- s) {
      if (characterCountMap.contains(character)) {
        characterCountMap(character) = characterCountMap(character) + 1
      } else {
        characterCountMap(character) = 1
      }
    }

    val loop = new Breaks;
    loop.breakable {
      for (character <- s) {
        if (characterCountMap(character) == 1) {
          result = character
          loop.break
        }
      }
    }
    result
  }

  println(findFirstNonRepeatCharacter("sfddsewwdf"))
  println(findFirstNonRepeatCharacter("gsfddsewwdf"))

  def addTwoNumbers(l1: ListNode, l2: ListNode) = {
    var p1 = l1
    var p2 = l2
    var carry = 0
    var p3: ListNode = null

    while (p1 != null || p2 != null) {
      var val1 = 0
      var val2 = 0
      if (p1 != null) {
        val1 = p1.value
        p1 = p1.next
      }

      if (p2 != null) {
        val2 = p2.value
        p2 = p2.next
      }

      val sum = val1 + val2 + carry
      val value = sum % 10
      carry = sum / 10
      p3 = ListNode(value, p3)
    }

    if (carry > 0)
      p3 = new ListNode(carry, p3)

    p3
  }

  val l1 = ListNode(3, ListNode(4, ListNode(8, ListNode(6, null))))
  val l2 = ListNode(7, ListNode(3, ListNode(7, ListNode(9, ListNode(5, ListNode(2, null))))))

  var l3 = addTwoNumbers(l1, l2)
  while (l3 != null) {
    print(l3.value + ",")
    l3 = l3.next
  }

  println()
  
  val l4 = ListNode(3, ListNode(4, ListNode(8, ListNode(6, null))))
  val l5 = ListNode(7, ListNode(3, ListNode(7, ListNode(9, null))))

  l3 = addTwoNumbers(l4, l5)
  while (l3 != null) {
    print(l3.value + ",")
    l3 = l3.next
  }
}

case class ListNode(value: Integer,
                    next: ListNode) {
}
